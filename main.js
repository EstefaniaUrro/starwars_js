function opcionesPlanetas() {
    document.getElementById("tablas").innerHTML = "";
    document.getElementById("titulo").innerHTML = "";
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "block";
    var op1 = document.getElementById("opcionPlanetas");
    var op2 = document.getElementById("opcionPersonajes");
    var op3 = document.getElementById("opcionPeliculas");
    var op4 = document.getElementById("opcionNaves");
    var op5 = document.getElementById("opcionVehiculos");
    var op6 = document.getElementById("opcionEspecies");
    if (op1.style.display === "none") {
        op1.style.display = "block";
        op2.style.display = "none";
        op3.style.display = "none";
        op4.style.display = "none";
        op5.style.display = "none";
        op6.style.display = "none";
    } else {
        op1.style.display = "none";
    }
}
function opcionesPersonajes() {
    document.getElementById("tablas").innerHTML = "";
    document.getElementById("titulo").innerHTML = "";
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "block";
    var op1 = document.getElementById("opcionPlanetas");
    var op2 = document.getElementById("opcionPersonajes");
    var op3 = document.getElementById("opcionPeliculas");
    var op4 = document.getElementById("opcionNaves");
    var op5 = document.getElementById("opcionVehiculos");
    var op6 = document.getElementById("opcionEspecies");
    if (op2.style.display === "none") {
        op2.style.display = "block";
        op1.style.display = "none";
        op3.style.display = "none";
        op4.style.display = "none";
        op5.style.display = "none";
        op6.style.display = "none";
    } else {
        op2.style.display = "none";
    }
}
function opcionesPeliculas() {
    document.getElementById("tablas").innerHTML = "";
    document.getElementById("titulo").innerHTML = "";
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "block";
    var op1 = document.getElementById("opcionPlanetas");
    var op2 = document.getElementById("opcionPersonajes");
    var op3 = document.getElementById("opcionPeliculas");
    var op4 = document.getElementById("opcionNaves");
    var op5 = document.getElementById("opcionVehiculos");
    var op6 = document.getElementById("opcionEspecies");
    if (op3.style.display === "none") {
        op3.style.display = "block";
        op1.style.display = "none";
        op2.style.display = "none";
        op4.style.display = "none";
        op5.style.display = "none";
        op6.style.display = "none";
    } else {
        op3.style.display = "none";
    }
}
function opcionesNaves() {
    document.getElementById("tablas").innerHTML = "";
    document.getElementById("titulo").innerHTML = "";
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "block";
    var op1 = document.getElementById("opcionPlanetas");
    var op2 = document.getElementById("opcionPersonajes");
    var op3 = document.getElementById("opcionPeliculas");
    var op4 = document.getElementById("opcionNaves");
    var op5 = document.getElementById("opcionVehiculos");
    var op6 = document.getElementById("opcionEspecies");
    if (op4.style.display === "none") {
        op4.style.display = "block";
        op1.style.display = "none";
        op2.style.display = "none";
        op3.style.display = "none";
        op5.style.display = "none";
        op6.style.display = "none";
    } else {
        op4.style.display = "none";
    }
}
function opcionesVehiculos() {
    document.getElementById("tablas").innerHTML = "";
    document.getElementById("titulo").innerHTML = "";
    var op1 = document.getElementById("opcionPlanetas");
    var op2 = document.getElementById("opcionPersonajes");
    var op3 = document.getElementById("opcionPeliculas");
    var op4 = document.getElementById("opcionNaves");
    var op5 = document.getElementById("opcionVehiculos");
    var op6 = document.getElementById("opcionEspecies");
    if (op5.style.display === "none") {
        op5.style.display = "block";
        op1.style.display = "none";
        op2.style.display = "none";
        op3.style.display = "none";
        op4.style.display = "none";
        op6.style.display = "none";
    } else {
        op5.style.display = "none";
    }
}
function opcionesEspecies() {
    document.getElementById("tablas").innerHTML = "";
    document.getElementById("titulo").innerHTML = "";
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "block";
    var op1 = document.getElementById("opcionPlanetas");
    var op2 = document.getElementById("opcionPersonajes");
    var op3 = document.getElementById("opcionPeliculas");
    var op4 = document.getElementById("opcionNaves");
    var op5 = document.getElementById("opcionVehiculos");
    var op6 = document.getElementById("opcionEspecies");
    if (op6.style.display === "none") {
        op6.style.display = "block";
        op1.style.display = "none";
        op2.style.display = "none";
        op3.style.display = "none";
        op4.style.display = "none";
        op5.style.display = "none";
    } else {
        op6.style.display = "none";
    }
}

//-------------------------------------------------------Planetas----------------------------------------------------------------------------

function planetas() {
    url = "http://swapi.dev/api/planets";
    $.getJSON(url, function (data) {
        salida = data.results;
        //console.log(salida);
        clasificarPlaneta(salida);
    });
}
function clasificarPlaneta(lista){
    var nombrePla = document.getElementById("nombrePlaneta").checked;
    var poblacionPla = document.getElementById("poblacionPlaneta").checked;
    var btn =  document.getElementById('btnPlanetas');
    if(nombrePla === true){
        salida = lista.sort(listarPorNombre);
        mostrarPlanetasPorNombre(salida);
    }
    else if(poblacionPla === true){
        salida = lista.sort(listarPorPoblacion);
        mostrarPlanetasPorPoblacion(salida);
    }
    else{
       btn.style.disable = true;
       document.getElementById("tablas").innerHTML = "<h2>ERROR! Debe seleccione una opción</h2>";
    }
}
function listarPorNombre(a, b) {
        if (a.name === b.name) return 0;
        if (a.name > b.name) return 1;
        return -1;
}
function listarPorPoblacion(a, b) {
    var a1 = parseInt(a.population);
    var b1 = parseInt(b.population);
    if (a1 === b1) return 0;
    if (a1 > b1) return 1;
    return -1;
}
function mostrarPlanetasPorNombre(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de planetas ordenado por nombre";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('POBLACIÓN'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('DIAMETRO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('CLIMA'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('TIERRA'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (planeta of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planeta.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planeta.population));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planeta.diameter));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planeta.climate));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planeta.terrain));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}
function mostrarPlanetasPorPoblacion(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de planetas ordenado por población";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('POBLACIÓN'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('DIAMETRO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('CLIMA'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('TIERRA'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (planetaPoblacion of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planetaPoblacion.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planetaPoblacion.population));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planetaPoblacion.diameter));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planetaPoblacion.climate));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(planetaPoblacion.terrain));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}

//----------------------------------------------------------Personajes-------------------------------------------------------------------

function personajes() {
    url = "http://swapi.dev/api/people/";
    $.getJSON(url, function (data) {
        salida = data.results;
       console.log(salida);
       clasificarPersonajes(salida);
    });
}
function clasificarPersonajes(lista){
    var nombrePerso = document.getElementById("nombrePersonaje").checked;
    var generoPerso = document.getElementById("generoPersonaje").checked;
    var alturaPerso = document.getElementById("alturaPersonaje").checked;
    var btn = document.getElementById("btnPersonajes");
    if(nombrePerso === true){
        salida = lista.sort(listarPorNombre);
        mostrarPersonajesPorNombre(salida);
    }
    else if(generoPerso === true){
        salida = lista.sort(listarPorGenero);
        mostrarPersonajesPorGenero(salida);
    }
    else if(alturaPerso === true){
        salida = lista.sort(listarPorAltura);
        mostrarPersonajesPorAltura(salida);
    }
    else{
        btn.style.disable = true;
        document.getElementById("tablas").innerHTML = "<h2>ERROR! Debe seleccione una opción</h2>";
     }
}
function listarPorGenero(a, b) {
    if (a.gender === b.gender) return 0;
    if (a.gender > b.gender) return 1;
    return -1;
}
function listarPorAltura(a, b) {
    if (a.height === b.height) return 0;
    if (a.height > b.height) return 1;
    return -1;
}
function mostrarPersonajesPorNombre(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de personajes ordenado por nombre";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('ALTURA'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('GÉNERO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('MASA'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('AÑO DE NACIMIENTO'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (personaje of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personaje.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personaje.height));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personaje.gender));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personaje.mass));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personaje.birth_year));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}
function mostrarPersonajesPorGenero(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de personajes ordenado por género ";
    var out = document.getElementById('tablas');
    out.innerHTML = " ";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('ALTURA'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('GÉNERO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('MASA'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('AÑO DE NACIMIENTO'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (personajeGenero of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajeGenero.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajeGenero.height));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajeGenero.gender));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajeGenero.mass));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajeGenero.birth_year));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}
function mostrarPersonajesPorAltura(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de personajes ordenado por altura ";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('ALTURA'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('GÉNERO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('MASA'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('AÑO DE NACIMIENTO'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (personajesAltura of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajesAltura.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajesAltura.height));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajesAltura.gender));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajesAltura.mass));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(personajesAltura.birth_year));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
    
}

//--------------------------------------------------------Peliculas-----------------------------------------------------------------

function peliculas() {
    url = "http://swapi.dev/api/films/";
    $.getJSON(url, function (data) {
        salida = data.results;
        //console.log(salida);
        clasificarPeliculas(salida);
    });
}
function clasificarPeliculas(lista){
    var tituloPeli = document.getElementById("tituloPelicula").checked;
    var episodiosPeli = document.getElementById("episodiosPelicula").checked;
    var directorPeli = document.getElementById("directorPelicula").checked;
    var fechaPeli = document.getElementById("fechaPelicula").checked;
    var btn =  document.getElementById('btnPeliculas');
    if(tituloPeli === true){
        salida = lista.sort(listarPorTitulo);
        mostrarPeliculasPorTitulo(salida);
    }
    else if(episodiosPeli === true){
        salida = lista.sort(listarPorEpisodios);
        mostrarPeliculasPorEpisodio(salida);
    }
    else if(directorPeli === true){
        salida = lista.sort(listarPorDirector);
        mostrarPeliculasPorDirector(salida);
    }
    else if(fechaPeli === true){
        salida = lista.sort(listarPorFecha);
        mostrarPeliculasPorFecha(salida);
    }
    else{
        btn.style.disable = true;
        document.getElementById("tablas").innerHTML = "<h2>ERROR! Debe seleccione una opción</h2>";
     }
}
function listarPorTitulo(a, b) {
    if (a.title === b.title) return 0;
    if (a.title > b.title) return 1;
    return -1;
}
function listarPorEpisodios(a, b) {
    if (a.episode_id === b.episode_id) return 0;
    if (a.episode_id > b.episode_id) return 1;
    return -1;
}
function listarPorDirector(a, b) {
    if (a.director === b.director) return 0;
    if (a.director > b.director) return 1;
    return -1;
}
function listarPorFecha(a, b) {
    if (a.release_date === b.release_date) return 0;
    if (a.release_date > b.release_date) return 1;
    return -1;
}
function mostrarPeliculasPorTitulo(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de películas ordenado por título";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('TITULO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('ID EPISODIO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('DIRECTOR'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('FECHA DE ESTRENO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('PRODUCTOR'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (pelicula of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(pelicula.title));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(pelicula.episode_id));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(pelicula.director));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(pelicula.release_date));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(pelicula.producer));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);   
}

function mostrarPeliculasPorEpisodio(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de películas ordenado por episodio";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('TITULO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('ID EPISODIO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('DIRECTOR'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('FECHA DE ESTRENO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('PRODUCTOR'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (peliculasEpisodio of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasEpisodio.title));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasEpisodio.episode_id));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasEpisodio.director));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasEpisodio.release_date));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasEpisodio.producer));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}

function mostrarPeliculasPorDirector(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de películas ordenado por director";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('TITULO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('ID EPISODIO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('DIRECTOR'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('FECHA DE ESTRENO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('PRODUCTOR'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (peliculasDirector of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasDirector.title));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasDirector.episode_id));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasDirector.director));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasDirector.release_date));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasDirector.producer));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}

function mostrarPeliculasPorFecha(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de películas ordenado por fecha";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('TITULO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('ID EPISODIO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('DIRECTOR'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('FECHA DE ESTRENO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('PRODUCTOR'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (peliculasFecha of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasFecha.title));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasFecha.episode_id));
        tr.appendChild(td);
        
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasFecha.director));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasFecha.release_date));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(peliculasFecha.producer));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}

//------------------------------------------------------------------------Naves--------------------------------------------------------------------------

function naves() {
    url = "http://swapi.dev/api/starships/";
    $.getJSON(url, function (data) {
        salida = data.results;
        //console.log(salida);
        clasificarNaves(salida);
    });
}
function clasificarNaves(lista){
    var nombreNav = document.getElementById("nombreNave").checked;
    var costeNav = document.getElementById("costeNave").checked;
    var pasajerosNav = document.getElementById("pasajerosNave").checked;
    var btn =  document.getElementById('btnNaves');
    if(nombreNav === true){
        salida = lista.sort(listarPorNombre);
        mostrarNavesPorNombre(salida);
    }
    else if(costeNav === true){
        salida = lista.sort(listarPorCoste);
        mostrarNavesPorCoste(salida);
    }
    else if(pasajerosNav === true){
        salida = lista.sort(listarPorPasajeros);
        mostrarNavesPorPasajeros(salida);
    }
    else{
        btn.style.disable = true;
        document.getElementById("tablas").innerHTML = "<h2>ERROR! Debe seleccione una opción</h2>";
     }
}
function listarPorCoste(a, b) {
    var a1 = parseInt(a.cost_in_credits);
    var b1 = parseInt(b.cost_in_credits);
    if (a1 === b1) return 0;
    if (a1 > b1) return 1;
    return -1;
}
function listarPorPasajeros(a, b) {
    if (a.passengers === b.passengers) return 0;
    if (a.passengers > b.passengers) return 1;
    return -1;
}

function mostrarNavesPorNombre(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de naves ordenado por nombre";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('COSTO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('PASAJEROS'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('CLASE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('MODELO'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (nave of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(nave.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(nave.cost_in_credits));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(nave.passengers));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(nave.starship_class));
        tr.appendChild(td);        
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(nave.model));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}
function mostrarNavesPorCoste(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de naves de ordenado por coste";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('COSTO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('PASAJEROS'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('CLASE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('MODELO'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (navesCoste of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesCoste.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesCoste.cost_in_credits));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesCoste.passengers));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesCoste.starship_class));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesCoste.model));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}
function mostrarNavesPorPasajeros(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de naves ordenado por pasajeros";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('COSTO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('PASAJEROS'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('CLASE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('MODELO'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (navesPasajeros of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesPasajeros.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesPasajeros.cost_in_credits));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesPasajeros.passengers));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesPasajeros.starship_class));
        tr.appendChild(td);
        
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(navesPasajeros.model));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}

//------------------------------------------------------------------vehiculos------------------------------------------------------------

function vehiculos() {
    url = "http://swapi.dev/api/vehicles/";
    $.getJSON(url, function (data) {
        salida = data.results;
        //console.log(salida);
        clasificarVehiculos(salida);
    });
}
function clasificarVehiculos(lista){
    var nombreVehi = document.getElementById("nombreVehiculo").checked;
    var largoVehi = document.getElementById("largoVehiculo").checked;
    var btn =  document.getElementById('btnNaves');
    if(nombreVehi === true){
        salida = lista.sort(listarPorNombre);
        mostrarVehiculoPorNombre(salida);
    }
    else if(largoVehi === true){
        salida = lista.sort(listarPorLargo);
        mostrarVehiculoPorLargo(salida);
    }
    else{
        btn.style.disable = true;
        document.getElementById("tablas").innerHTML = "<h2>ERROR! Debe seleccione una opción</h2>";
     }
}
function listarPorLargo(a, b) {
    var a1 = parseInt(a.created);
    var b1 = parseInt(b.created);
    if (a1 === b1) return 0;
    if (a1 > b1) return 1;
    return -1;
}
function mostrarVehiculoPorNombre(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de vehiculos ordenado por nombre";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('LONGITUD'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('COSTO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('PASAJEROS'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('MODELO'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (vehiculo of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculo.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculo.length));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculo.cost_in_credits));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculo.passengers));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculo.created));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}
function mostrarVehiculoPorLargo(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de vehiculos ordenado por lonngitud";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('LONGITUD'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('COSTO'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('PASAJEROS'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('MODELO'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (vehiculosLargo of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculosLargo.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculosLargo.length));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculosLargo.cost_in_credits));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculosLargo.passengers));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(vehiculosLargo.model));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}

function especies() {
    url = "http://swapi.dev/api/species/";
    $.getJSON(url, function (data) {
        salida = data.results;
        //console.log(salida);
        clasificarEspecies(salida);
    });
}
function clasificarEspecies(lista){
    var nombreEspecie = document.getElementById("nombreEspecie").checked;
    var btn =  document.getElementById('btnNaves');
    if(nombreEspecie === true){
        salida = lista.sort(listarPorNombre);
        mostrarEspeciePorNombre(salida);
    }else{
        btn.style.disable = true;
        document.getElementById("tablas").innerHTML = "<h2>ERROR! Debe seleccione una opción</h2>";
     }
}
function mostrarEspeciePorNombre(lista){
    var contendor = document.getElementById("contenedor");
    contendor.style.display = "none";
    document.getElementById("titulo").innerHTML = "Listado de especies ordenado por nombre";
    var out = document.getElementById('tablas');
    out.innerHTML = "";
    var tbl = document.createElement('table');
    tbl.setAttribute('class','table');
    var the = document.createElement('thead');
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('NOMBRE'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('CLASIFICACIÓN'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('ALTURA'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('DESIGNACIÓN'));
    tr.appendChild(th);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode('LENGUAJE'));
    tr.appendChild(th);
    the.appendChild(tr);
    var tbdy = document.createElement('tbody');
    for (especie of lista) {
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(especie.name));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(especie.classification));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(especie.average_height));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(especie.designation));
        tr.appendChild(td);
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(especie.language));
        tr.appendChild(td);
        tbdy.appendChild(tr);
    };
    tbl.appendChild(tbdy);
    tbl.appendChild(the);
    out.appendChild(tbl);
}
       
